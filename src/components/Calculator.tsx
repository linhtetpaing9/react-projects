import React, { useEffect, useState } from 'react';


function useSumByTwo(value: any){
    useEffect(() => {
        if( value > 10){
            console.log(`the ${value} is greater than 10`);
        }
    }, [value])
    return value;
}


const Calculator = () => {
    const [ value, setValue ] = useState(0);

    console.log(useSumByTwo(value));

    function handleClick(){
        setValue(value + 5)
    }


    return (
        <>
        <input type="text" value={value} />
        <button onClick={handleClick}>Click Me to Sum 5</button>
        <h1>{value}</h1>
        </>
    )
}

export default Calculator;
