import React , { FunctionComponent } from "react";

type IncomeFormProps = {
  title?: string
}

const IncomeForm: FunctionComponent<IncomeFormProps> = (props) => <div>{props.title}></div>;

export default IncomeForm;
