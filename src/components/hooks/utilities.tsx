import React, { useEffect, useState } from 'react';


// do it for every method
export function useFetch(endpoint: string, query?: {} | any, setting?: {}){
    const [ result , setResult ] = useState({} as any);
    useEffect(() => {
      let queryString = Object.keys(query).map(key => `${key}=${query[key]}`).join("&");
      try {
        const fetchData = async () => {
          const response = await fetch(
            `${endpoint}?${queryString}`,
            setting
          );
          const data = await response.json();
          setResult(data);
        }
        fetchData();
      } catch (error) {
        return error;        
      }
    }, [ query ]);
    return result
}

