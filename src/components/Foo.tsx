import React , { useContext } from 'react';
import { ThemeContext, NestedContext } from "../data/Context";

export default function Foo(){
  const value: any = useContext(ThemeContext);
  const sValue: any = useContext(NestedContext);

  return (
    <>
      <h1>{value.name}</h1>
      <h1>{sValue}</h1>
      <h1>{value.address}</h1>
      <h1>{value.contact}</h1>
    </>
  )
}