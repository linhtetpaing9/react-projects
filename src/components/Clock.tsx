import React, { Component } from 'react';

type ClockState = {
    time: Date
}

class Clock extends Component<{}, ClockState>{
    tick(){
        this.setState({
            time: new Date()
        });
    }

    componentDidMount(){
        setInterval(() => {
            this.tick()
        }, 1000)
    }

    componentWillMount(){
        this.tick();
    }

    render(){
        return <p>The current time is {this.state.time.toLocaleTimeString()}</p>
    }
}

export default Clock;