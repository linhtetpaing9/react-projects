import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Home from './pages/HomePage';
import Income from './pages/IncomePage';
import Expense from './pages/ExpensePage';
import TodoLists from './pages/TodoListsPage';
import StatePage from './pages/StatePage';
import MovieSearch from './pages/MovieSearch';
import { Layout, Menu } from "antd";
const { Header, Content, Footer } = Layout;


const App: React.FC = () => {
  return (
    <Router>
      <Layout>
          <Header style={{ position: "fixed", zIndex: 1, width: "100%" }}>
            <div className="logo" />
            <Menu
              theme="dark"
              mode="horizontal"
              defaultSelectedKeys={["1"]}
              style={{ lineHeight: "64px" }}
            >
              <Menu.Item key="1">
                <Link to="/">Home</Link>
              </Menu.Item>
              <Menu.Item key="2">
                <Link to="/income">Income</Link>
              </Menu.Item>
              <Menu.Item key="3">
                <Link to="/expense">Expense</Link>
              </Menu.Item>
              <Menu.Item key="4">
                <Link to="/todolists">TodoLists</Link>
              </Menu.Item>
              <Menu.Item key="5">
                <Link to="/state">State</Link>
              </Menu.Item>
              <Menu.Item key="6">
                <Link to="/movie">Movie</Link>
              </Menu.Item>
            </Menu>
          </Header>
        <Content style={{ padding: "20px 50px", marginTop: 64 }}>
          <div style={{ background: "#fff", padding: 24, minHeight: 380 }}>
            <Route path="/" exact component={Home} />
            <Route path="/income" component={Income} />
            <Route path="/expense" component={Expense} />
            <Route path="/todolists" component={TodoLists} />
            <Route path="/state" component={StatePage} />
            <Route path="/movie" component={MovieSearch} />
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Created By Robert
        </Footer>
      </Layout>
    </Router>
  );
}

export default App;
