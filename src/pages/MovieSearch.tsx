import React , { useState } from 'react';
import { Row , Col , Card, Icon, Avatar } from 'antd';
import { useFetch } from '../components/hooks/utilities';

const { Meta } = Card;

const MovieSearch = () => {
    const endpoint = "http://www.omdbapi.com";
    const key = "4a3b711b";
    const setting = {
        method: "GET",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        }
      };
    const [query, setQuery] = useState({t: 'G', apiKey: key} as { t: any, apiKey?: any });
    const result = useFetch(endpoint, query, setting);

    return (
      <Row gutter={24}>
        <Col span={24} offset={10}>
          {/* autocomplete here */}
          <input type="text" onChange={(e) => setQuery({t: e.target.value, apiKey: key})}/>
        </Col>
        <Col span={24}>
          <Card
            style={{ width: 300 }}
            cover={
              <img
                alt="example"
                src={result["Poster"]}
              />
            }
            actions={[
              <Icon type="setting" key="setting" />,
              <Icon type="edit" key="edit" />,
              <Icon type="ellipsis" key="ellipsis" />
            ]}
          >
            <Meta
              avatar={
                <Avatar src={result["Poster"]} />
              }
              title={result["Title"]}
              description={result["Genre"]}
            />
          </Card>
        </Col>
      </Row>
    );
}

export default MovieSearch;