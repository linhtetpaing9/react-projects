import React from 'react';
import * as Name from '../data/name.js';
import Clock from '../components/Clock';

const HomePage = () => {
    return (
        <>
            <h1>{Name.name}</h1>
            <h1>{Name.gender}</h1>
            <Clock />
        </>
    )
}

export default HomePage;
