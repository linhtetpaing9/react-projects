import React , { useState, useEffect }from "react";

const ExpensePage: React.FC = () => {
  const [ greeting , setGreeting ] = useState('Hello React!');
  const handleChange = (event: any ) => setGreeting(event.target.value);
  useEffect(() => {
    // console.log(ExpensePage);
    console.log(greeting);
  }, [ greeting ])
  return (
    <div>
      { console.log(ExpensePage) }
      {/* <h1>{greeting}</h1> */}
      <Input value={greeting} handleChange={handleChange} />
    </div>
  )
};
type InputInterface = {
  value: any,
  handleChange: any 
}

const Input: React.FC<InputInterface> = ({ value, handleChange }) => {
  const ref: any = React.createRef();
  useEffect(() => {
    ref.current.focus(); 
  }, []);
  return (
    <input type="text" value={value} onChange={handleChange} ref={ref}/>
  )
}

export default ExpensePage;
