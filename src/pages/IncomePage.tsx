import React from 'react';
import IncomeForm from '../components/IncomeForm';
import Calculator from '../components/Calculator';
import Foo from '../components/Foo';
import { ThemeContext, NestedContext } from '../data/Context';

const IncomePage: React.FC = () => (
  <>
    <Calculator />
    <IncomeForm />
    <ThemeContext.Provider
      value={{
        name: "Lin Htet Paing",
        address: "No(76)",
        contact: "09250359280"
      }}
    >
      <NestedContext.Provider value={'Lin'}>
        <Foo />
      </NestedContext.Provider>
    </ThemeContext.Provider>
  </>
);

export default IncomePage;