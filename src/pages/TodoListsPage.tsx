import React , { useEffect , useState } from 'react';

function useFetch(endpoint: any, query: any){
    const [ response, setResponse ] = useState({});
    useEffect(() => {
        // setResponse(fetch(endpoint))
    }, [ query ]);
    return response;
}

const TodoListsPage = () => {
    const [ count, setCount ] = useState(0);
    const [ number, setNumber ] = useState(0);
    const response = useFetch("https://jsonplaceholder.typicode.com/posts", {});

    useEffect(() => {
        console.log(response);
        console.log('after render...');
        console.log(count);
        // return () => setNumber(count);
    }, [ number ])
    return (
        <>
            <h1>{count}</h1>
            <button onClick={() => setCount(count + 1)}>Click me</button>
            <button onClick={() => setNumber(number + 1)}>Click me</button>
        </>
    )
}

export default TodoListsPage;
