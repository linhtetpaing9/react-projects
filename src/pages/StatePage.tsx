import React from 'react';

class StatePage extends React.Component<any, any>{
    constructor(props: any){
        super(props);
        this.state = {
            name: 'Lin Htet Paing',
            phone: {
                office: '0123123',
                home: '3434'
            }
        }
    }

    render(){
        return <h1>{this.state.name}</h1>;
    }
}

export default StatePage;